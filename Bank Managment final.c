#include<stdio.h>
#include<conio.h>
#include<string.h>
#define size 100
int i=0,ch,acc,j;

struct customer {
    char customer_name[50];     // customer name
    char customer_add[100];     // customer address
    int customer_id;          // customer id
} customer[size];              //stores 100 customer records

struct branch {
    char branch_name[50];     // branch name
} branch[size];              // stores branch records

struct account {
    char acc_type[20];   // account type
    int acc_no;          // account number
    int balance;        //current balance in account
} account[size];              // stores accounts

int main() {

    for(;;) {                                               // infinite loop
        printf("\nEnter Your Choice\n");
        printf("1 for adding a record.\n");
        printf("2 for viewing a record.\n");
        printf("3 for deleting a record.\n");
        printf("4 for updating details.\n");
        printf("5 to Exit the program \n\n");
        scanf("%d", &ch);

        switch(ch) {                                        // switch statement
            case 1: {
                create();
                break;
            }

            case 2: {
                view();
                break;
            }

            case 3: {
                del();
                break;
            }

            case 4: {
                update();
                break;
            }

            case 5: {
                break;
            }

            default: {
                printf("Please select a valid option");
                break;
            }

        }
    }
}

void create() {
    printf("Enter the name of Customer: ");
    getchar();
    gets(customer[i].customer_name);
    printf("Enter the address of Customer: ");
    gets(customer[i].customer_add);
    printf("Enter the Customer id: ");
    scanf("%d", &customer[i].customer_id);
    printf("Enter the Branch: ");
    getchar();
    gets(branch[i].branch_name);
    printf("Account type: ");
    gets(account[i].acc_type);
    printf("account number: ");
    scanf("%d", &account[i].acc_no);
    printf("Enter the Amoount of Balance: ");
    scanf("%d", &account[i].balance);
    printf("\n\n");
    i++;
    printf("*************************************************");
}

void view () {
    printf("Which Customers' details do you want to view?Please enter his/her account number: ");
    scanf("%d", &acc);
    for (j = 0;; j++) {
        if (account[j].acc_no == acc)
            break;
    }

    printf("Customer Name: %s\n", customer[j].customer_name);
    printf("Customer Address: %s\n", customer[j].customer_add);
    printf("Customer Id: %d\n", customer[j].customer_id);
    printf("Branch: %s\n", branch[j].branch_name);
    printf("Account Type: %s\n", account[j].acc_type);
    printf("Account Number: %d\n", account[j].acc_no);
    printf("Balance: %d\n", account[j].balance);
    printf("\n\n");
    printf("*************************************************");
}


void del () {
    printf("Which Customers' details do you want to delete?Please enter his/her account number: ");
    scanf("%d", &acc);
    for (j = 0; j < i; j++) {
        if (account[j].acc_no == acc)
            break;
    printf("*************************************************");
    }
}
void update() {
    printf("Which Customer details do you want to update? Please enter his/her account number: ");
    scanf("%d", &acc);
    for (j = 0;; j++) {
        if (account[j].acc_no == acc)
            break;
    }
    printf("Customer Name: ");
    getchar();
    gets(customer[j].customer_name);
    printf("Customer Address: ");
    gets(customer[j].customer_add);
    printf("Customer Id: ");
    scanf("%d", &customer[j].customer_id);
    printf("Branch: ");
    getchar();
    gets(branch[j].branch_name);
    printf("Account type: ");
    gets(account[j].acc_type);
    printf("Amount of Balance: ");
    scanf("%d", &account[j].balance);
    printf("\n\n");
    printf("*************************************************");
}


